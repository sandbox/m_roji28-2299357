CONTENTS OF THIS FILE
---------------------
 * Overview
 * Installation
 * How to use


Overview
--------
Give link to clone menu item.


Installation
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


How to use
----------
Visit overview menu form,
admin/structure/menu/manage/(MENU) which (MENU) is your machine name menu,
you'll see the clone link.
