<?php

/**
 * @file
 * Admin page.
 */

/**
 * Return form array.
 *
 * Get default form from menu_edit_item,
 * then add value to get behaviour same as create new item.
 */
function menu_item_clone_form($form, &$form_state) {
  $menu_link = $form_state['build_info']['args'][0];
  module_load_include('inc', 'menu', 'menu.admin');
  $form = drupal_get_form('menu_edit_item', 'edit', $menu_link, NULL);

  // Remove delete.
  unset($form['actions']['delete']);

  // Remove #value.
  menu_item_clone_remove_value($form);

  // Set clone value.
  $form['actions']['submit']['#value'] = t('Save');

  // Empty mlid is same as new.
  $form['mlid']['#value'] = 0;

  // Set module as menu, so we can delete soon.
  $form['module']['#value'] = 'menu';

  return $form;
}

/**
 * Remove property #value.
 *
 * If element form has property #value, it means element can't be change
 * se we can't get custom value at $form_state[values].
 */
function menu_item_clone_remove_value(&$form) {
  if (isset($form['#default_value']) && isset($form['#value']) && $form['#value'] == $form['#default_value']) {
    unset($form['#value']);
  }
  // Recursive.
  foreach (element_children($form) as $child) {
    menu_item_clone_remove_value($form[$child]);
  }
}
